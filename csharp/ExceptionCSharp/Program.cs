﻿using System;

namespace Ariane5
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                var rocket = new Rocket();
                rocket.launch();
            } catch
            {
                Console.WriteLine("Loosed control of the rocket. Initating self destruction.");
                Environment.Exit(-1);
            }
        }
    }
}
 