﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Ariane5
{
    class Engine
    {
        public float Thrust { get; set; }

        public Engine()
        {
            Thrust = 0;
        }
    }
}
