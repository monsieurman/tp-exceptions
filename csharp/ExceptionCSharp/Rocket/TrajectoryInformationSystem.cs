﻿using System;
using System.Reactive.Linq;
using System.Reactive.Subjects;

namespace Ariane5
{
    public class TrajectoryInformationSystem
    {
        private Subject<TrajectoryInformation> informationsSubject = new Subject<TrajectoryInformation>();

        private readonly PositionProbe posProbe;
        private readonly VelocityProbe velProbe;
        private readonly GyroProbe gyroProbe;

        public TrajectoryInformationSystem(PositionProbe posProbe, VelocityProbe velProbe, GyroProbe gyroProbe)
        {
            this.posProbe = posProbe;
            this.velProbe = velProbe;
            this.gyroProbe = gyroProbe;
        }

        public void AskDataFromProbes()
        {
            Console.WriteLine("Pushing new data from probes.");
            var trajectoryInformation = new TrajectoryInformation(
                this.posProbe.Position,
                this.velProbe.Velocity,
                this.gyroProbe.Rotation
            );
            this.informationsSubject.OnNext(trajectoryInformation);
        }

        public IObservable<TrajectoryInformation> GetObservable()
        {
            return informationsSubject.AsObservable();
        }
    }
}
