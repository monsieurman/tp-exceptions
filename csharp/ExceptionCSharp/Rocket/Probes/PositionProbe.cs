﻿using System.Collections.Generic;
using System.Numerics;

namespace Ariane5
{
    public class PositionProbe
    {
        int count = 0;
        // Valeurs d'exemple non realistes, mais correpondantes au probleme.
        // NE PAS CHANGER !
        List<Vector3> values = new List<Vector3>()
        {
            new Vector3(0F, 0F, 0),
            new Vector3(0F, 1F, 11),
            new Vector3(0F, 2F, 14),
            new Vector3(0F, 8, 20),
            new Vector3(0F, 16, 40),
            new Vector3(0F, 32, 80),
            new Vector3(0F, 85, 160),

        };
        public Vector3 Position
        {
            get
            {
                var val = values[count];
                count++;
                return val;
            }
        }
    }
}
