﻿using System.Collections.Generic;
using System.Numerics;

namespace Ariane5
{
    public class VelocityProbe
    {
        int count = 0;
        // Valeurs d'exemple non realistes, mais correpondantes au probleme.
        // NE PAS CHANGER !
        List<Vector3> values = new List<Vector3>()
        {
            new Vector3(0F, 0F, 0),
            new Vector3(0F, 1F, 11),
            new Vector3(0F, 2F, 1239),
            new Vector3(0F, 4F, 15320),
            new Vector3(0F, 8F, 22994),
            new Vector3(0F, 20F, 34902),
            new Vector3(0F, 20F, 38000),

        };
        public Vector3 Velocity
        {
            get {
                var val = values[count];
                count++;
                return val;
            } 
        }
    }
}
