﻿using System.Collections.Generic;
using System.Numerics;



namespace Ariane5
{
    public class GyroProbe
    {
        int count = 0;
        // Valeurs d'exemple non realistes, mais correpondantes au probleme.
        // NE PAS CHANGER !
        List<Vector3> values = new List<Vector3>()
        {
            new Vector3(0F, 0F, 0),
            new Vector3(0F, 1F, 11),
            new Vector3(0F, 2F, 12),
            new Vector3(0F, 4F, 12),
            new Vector3(0F, 8F, 12),
            new Vector3(0F, 20F, 12),
            new Vector3(0F, 20F, 15),

        };
        public Vector3 Rotation
        {
            get
            {
                var val = values[count];
                count++;
                return val;
            }
        }
    }
}
