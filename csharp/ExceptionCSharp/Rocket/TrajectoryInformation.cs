﻿using System.Numerics;

namespace Ariane5
{
    public class TrajectoryInformation
    {
        public Vector3 Position { get; }
        public Vector3 Velocity { get; }
        public Vector3 Rotation { get; }

        public TrajectoryInformation(Vector3 position, Vector3 velocity, Vector3 rotation)
        {
            Position = position;
            Velocity = velocity;
            Rotation = rotation;
        }
    }
}
