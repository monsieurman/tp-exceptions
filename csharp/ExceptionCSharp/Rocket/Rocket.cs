﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Ariane5
{
    public class Rocket
    {
        // Engines
        Engine leftEngine = new Engine();
        Engine rightEngine = new Engine();

        // Control systems
        TrajectoryCorrectionSystem trajectoryCorrection;
        TrajectoryInformationSystem trajectoryInformation;

        public Rocket()
        {
            // Probes
            this.trajectoryInformation = new TrajectoryInformationSystem(new PositionProbe(), new VelocityProbe(), new GyroProbe());
            this.trajectoryCorrection = new TrajectoryCorrectionSystem(this.trajectoryInformation.GetObservable(), leftEngine, rightEngine);
        }

        public void launch()
        {
            while(true)
            {
                this.trajectoryInformation.AskDataFromProbes();
            }
        }
    }
}
