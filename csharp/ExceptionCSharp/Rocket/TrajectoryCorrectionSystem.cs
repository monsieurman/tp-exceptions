﻿using System;

namespace Ariane5
{
    class TrajectoryCorrectionSystem
    {
        private readonly Engine engineLeft;
        private readonly Engine engineRight;

        public TrajectoryCorrectionSystem(IObservable<TrajectoryInformation> trajectoryInformations, Engine engineLeft, Engine engineRight)
        {
            trajectoryInformations.Subscribe(correctTrajectory);
            this.engineLeft = engineLeft;
            this.engineRight = engineRight;
        }

        void correctTrajectory(TrajectoryInformation trajectoryInformation)
        {
            short velZ = Convert.ToInt16(trajectoryInformation.Velocity.Z);
            short velY = Convert.ToInt16(trajectoryInformation.Velocity.Y);
            short velX = Convert.ToInt16(trajectoryInformation.Velocity.X);

            engineLeft.Thrust = (float)(velX * trajectoryInformation.Rotation.X / Math.PI);
            engineRight.Thrust = (float)(velZ * trajectoryInformation.Rotation.Z / Math.PI);
            engineLeft.Thrust += (float)(velY * 0.3);
            engineRight.Thrust += (float)(velY * 0.3);
        }
    }
}
