# Cours exceptions

Slides du premier cours : https://slides.com/mrman/deck-23b9d6

## TP C#

Lancer la solution Ariane5, trouver la raison de l'erreur.

## TP PHP

- Installer xampp, ou un autre moyen d'avoir PHP et Apache sur votre ordinateur
- Créer les exceptions qui vont être utiliser pour la classe Student ci-dessous.
- Créer une classe Student
    - Avec un constructeur qui prends les paramètres
        - FirstName, lever une exception `InvalidNameException` s'il dépasse 25 charactères 
        - LastName, même chose que pour FirstName `InvalidNameException`
        - Age, lever une exception `InvalidAgeException` s'il est inférieur à 15 ou supérieur à 25
        - Email, lever une exception `InvalidEmailException` s'il est invalide.
- Créer une classe `LineError`
    - Un constructeur avec en paramètre :
        - Line, la ligne de l'erreur dans le fichier.
        - ExceptionType, le type de l'exception. (`string`)
        - Message, le message de l'exception.
    - Une méthode `__tostring` permettant l'affichage de l'erreur comme ci-dessous.
- Afin de convertir les anciennes erreurs PHP(avant PHP 5) en exceptions, copier ce code en haut de votre fichier php :
```php
function exception_error_handler($severity, $message, $file, $line) {
    if (!(error_reporting() & $severity)) {
        // This error code is not included in error_reporting
        return;
    }
    throw new ErrorException($message, 0, $severity, $file, $line);
}
set_error_handler("exception_error_handler");
```
- Lire le fichier student.csv ligne par ligne
    - Gérer l'erreur si le fichier n'a pas pu être trouver.
- Pour chaque ligne créer un élève
    - Stocker dans un tableau chaque numéro de ligne créant une erreur, avec l'erreur associé
    - Stocker dans un tableau chaque élève ayant put être créer
- Logger le nombre d'élève correct
- Afficher chaque ligne incorrect avec l'erreur associé. Ex : 

![Example output](php/example_output.png)